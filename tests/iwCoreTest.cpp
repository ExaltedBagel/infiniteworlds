//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#include <gtest/gtest.h>
#include "constants.hpp"
#include "infiniteWorlds.h"
#include "math.h"
#include <array>
#include <algorithm>
#include <exception>
#include <string>

#include <filesystem>

namespace fs = std::filesystem;

class TileBucketTest : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        auto ws = fs::temp_directory_path() / "IWTest";
        defaultWS = ws.generic_string();
        fs::remove_all(defaultWS);
        ClearTileBucket();
        SetWorkspace(defaultWS.c_str());
        std::error_code e;
        fs::create_directory(ws, e);
    }

    virtual void TearDown()
    {
        ClearTileBucket();
    }
public:
    std::string defaultWS;
};

TEST_F(TileBucketTest, GeneratesCorrectSize) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);

    FetchTiles(tileArray.data(), tileArray.size());

    bool atLeastOneTrue = false;

    for(const auto& tile : tileArray) {
        atLeastOneTrue |= tile.content;
    }

    EXPECT_TRUE(atLeastOneTrue);
}

TEST_F(TileBucketTest, GeneratesSomethingDiverse) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);

    FetchTiles(tileArray.data(), tileArray.size());

    bool allTrue = true;
    bool allFalse = true;

    for(size_t i = 0; i < tileArray.size(); ++i) {
        allTrue &= tileArray[i].content;
        allFalse &= !tileArray[i].content;
    }

    EXPECT_FALSE(allFalse);
    EXPECT_FALSE(allTrue);
}

TEST_F(TileBucketTest, GeneratesDeterministic) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);
    std::vector<Tile> tileArray2;
    tileArray2.resize(TILE_ARRAY_SIZE);

    FetchTiles(tileArray.data(), tileArray.size());
    ClearTileBucket();
    FetchTiles(tileArray2.data(), tileArray2.size());

    for(size_t i = 0; i < tileArray.size(); ++i) {
        EXPECT_EQ(tileArray[i].content, tileArray2[i].content);
    }
}

TEST_F(TileBucketTest, SetScope) {
    SetScope(0, 0);
    EXPECT_EQ(0, GetBaseXCoord());
    EXPECT_EQ(0, GetBaseYCoord());

    SetScope(2, 0);
    EXPECT_EQ(2 * CHUNK_SIZE, GetBaseXCoord());
    EXPECT_EQ(0, GetBaseYCoord());

    SetScope(0, 2);
    EXPECT_EQ(0, GetBaseXCoord());
    EXPECT_EQ(2 * CHUNK_SIZE * CHUNK_X_COUNT, GetBaseYCoord());

    SetScope(1, 2);
    EXPECT_EQ(CHUNK_SIZE, GetBaseXCoord());
    EXPECT_EQ(2 * CHUNK_SIZE * CHUNK_X_COUNT, GetBaseYCoord());
}

TEST_F(TileBucketTest, SetScopeDeterministic) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);
    std::vector<Tile> tileArray2;
    tileArray2.resize(TILE_ARRAY_SIZE);

    ClearTileBucket();
    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());
    ClearTileBucket();

    SetScope(0, 0);
    FetchTiles(tileArray2.data(), tileArray2.size());
    ClearTileBucket();

    size_t differences = 0;
    for(size_t i = 0; i < tileArray.size(); ++i) {
        if(tileArray[i].content != tileArray2[i].content)
        {
            ++differences;
        }
    }

    EXPECT_EQ(differences, 0) << "0,0 non deterministic";

    SetScope(1, 0);
    FetchTiles(tileArray.data(), tileArray.size());
    ClearTileBucket();

    SetScope(1, 0);
    FetchTiles(tileArray2.data(), tileArray2.size());
    ClearTileBucket();

    differences = 0;
    for(size_t i = 0; i < tileArray.size(); ++i) {
        if(tileArray[i].content != tileArray2[i].content)
        {
            ++differences;
        }
    }

    EXPECT_EQ(differences, 0) << "1,1 non deterministic";
}

TEST_F(TileBucketTest, CopyIsDeep) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);

    try {
        SetScope(0, 0);
    }
    catch (...){
        ASSERT_TRUE(false);
    }

    FetchTiles(tileArray.data(), tileArray.size());

    size_t p1 = 0;
    for(const auto& tile : tileArray) {
        p1 += tile.content;
    }

    try {
        SetScope(12, 20);
    }
    catch (...){
        ASSERT_TRUE(false);
    }

    size_t p2 = 0;
    for(const auto& tile : tileArray) {
        p2 += tile.content;
    }

    EXPECT_EQ(p1, p2);
}

TEST_F(TileBucketTest, SetScopeActuallyMovesMap) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);
    std::vector<Tile> tileArray2;
    tileArray2.resize(TILE_ARRAY_SIZE);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());

    size_t p1 = 0;
    for(const auto& tile : tileArray) {
        p1 += tile.content;
    }

    SetScope(12, 20);
    FetchTiles(tileArray2.data(), tileArray2.size());

    size_t p2 = 0;
    for(const auto& tile : tileArray2) {
        p2 += tile.content;
    }

    EXPECT_NE(p1, p2);

    bool allEqual = true;
    for(size_t i = 0; i < tileArray.size(); ++i) {
        allEqual &= (tileArray[i].content == tileArray2[i].content);
    }
    EXPECT_FALSE(allEqual);
}

TEST_F(TileBucketTest, FetchReordersData) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);
    std::vector<Tile> tileArray2;
    tileArray2.resize(TILE_ARRAY_SIZE);

    ClearTileBucket();
    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());

    size_t p1 = 0;
    for(const auto& tile : tileArray) {
        p1 += tile.content;
    }

    SetScope(1, 0);
    SetScope(0, 0);
    FetchTiles(tileArray2.data(), tileArray2.size());

    size_t p2 = 0;
    for(const auto& tile : tileArray2) {
        p2 += tile.content;
    }

    EXPECT_EQ(p1, p2);

    bool allEqual = true;
    for(size_t i = 0; i < tileArray.size(); ++i) {
        allEqual &= (tileArray[i].content == tileArray2[i].content);
    }
    EXPECT_TRUE(allEqual);
}

TEST_F(TileBucketTest, ChangeTileInScope) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());

    auto initial = tileArray[1].content;
    auto changeTo = !tileArray[1].content;
    tileArray[1].content = changeTo;

    NotifyTileChanged(&tileArray[1], 1, 0);

    EXPECT_EQ(CountChanged(), 1);
}

TEST_F(TileBucketTest, ChangeTileInScopeSerialized) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());

    auto initial = tileArray[1].content;
    auto changeTo = !tileArray[1].content;
    tileArray[1].content = changeTo;

    NotifyTileChanged(&tileArray[1], 1, 0);

    EXPECT_EQ(CountChanged(), 1);

    SetScope(1, 0);
    EXPECT_EQ(CountChanged(), 0);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());
    EXPECT_EQ(CountChanged(), 1);
    EXPECT_EQ(tileArray[1].content, changeTo);
}

TEST_F(TileBucketTest, ChangeTileInScopeSerializedRepeated) {
    std::vector<Tile> tileArray;
    tileArray.resize(TILE_ARRAY_SIZE);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());

    auto initial = tileArray[1].content;
    auto changeTo = !tileArray[1].content;
    tileArray[1].content = changeTo;

    NotifyTileChanged(&tileArray[1], 1, 0);

    EXPECT_EQ(CountChanged(), 1);

    SetScope(1, 0);
    EXPECT_EQ(CountChanged(), 0);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());
    EXPECT_EQ(CountChanged(), 1);
    EXPECT_EQ(tileArray[1].content, changeTo);

    SetScope(1, 0);
    EXPECT_EQ(CountChanged(), 0);

    SetScope(0, 0);
    FetchTiles(tileArray.data(), tileArray.size());
    EXPECT_EQ(CountChanged(), 1);
    EXPECT_EQ(tileArray[1].content, changeTo);
}