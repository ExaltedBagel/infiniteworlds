# Contributing
At this stage, feel free to use the code, but I am not looking for contributions 
since the interface of the project is very unstable at this point in time. Once
the project direction is more stable and defined, contributions will be open.

## Feature requests
Feel free to log feature requests / suggestions in the issues section. Add the label "suggestion" to your request
