cmake_minimum_required(VERSION 3.21)

find_package(OpenMP REQUIRED)

# add the executable
add_library(IW SHARED)

target_include_directories(IW PRIVATE "${CMAKE_SOURCE_DIR}/thirdparty/sivPerlinNoise")

# add the sources
target_sources(IW PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}/constants.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/loadedChunk.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/tile.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/tileBucket.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/tileChange.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/infiniteWorlds.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/defaultGenerator.cpp"
)