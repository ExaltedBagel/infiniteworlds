//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#pragma once

#include "tileChange.hpp"

#include <vector>
#include <filesystem>

namespace fs = std::filesystem;

struct LoadedChunk {
    LoadedChunk();
    LoadedChunk(int64_t x, int64_t y, size_t infoOffset = 0, size_t bufferOffset = 0);
    LoadedChunk(const LoadedChunk& other);
    LoadedChunk& operator=(const LoadedChunk& other);

    void Set(Tile* tile, int64_t x, int64_t y);

    void Save(fs::path rootPath, int layer) const;
    void Load(fs::path rootPath, int layer);

    // List of changes contained in the chunk
    std::vector<TileChange> changes;

    // Chunk X value
    int64_t x;

    // Chunk Y value
    int64_t y;

    // Offset in the bucket LoadedChunk vector
    size_t infoOffset;

    // Offset where info is contained in the Tile array
    size_t bufferOffset;
};