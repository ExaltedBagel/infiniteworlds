//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#include "infiniteWorlds.h"
#include "tile.h"
#include "defaultGenerator.h"
#include "tileBucket.hpp"
#include "constants.hpp"

#include <array>

static std::array<TileBucket, DEF_MAP_LAYERS> TILE_BUCKETS = {
    TileBucket(0), TileBucket(1), TileBucket(2), TileBucket(3)
};

void SetGenerator() {
    
}

void InitTileBucket(int layer) {
    TILE_BUCKETS[layer].Init();
}

void ClearTileBucket(int layer) {
    TILE_BUCKETS[layer].Clear();
}

void SetScope(int64_t chunkX, int64_t chunkY) {
    for(int i = 0; i < TILE_BUCKETS.size(); ++i) {
        TILE_BUCKETS[i].SetScope(chunkX, chunkY);
    }
}

int64_t GetBaseXCoord() {
    return TILE_BUCKETS[0].BaseX();
}

int64_t GetBaseYCoord() {
    return TILE_BUCKETS[0].BaseY();
}

void FetchTiles(Tile* tileArray, size_t bufferSize, int layer) {
    TILE_BUCKETS[layer].FetchTiles(tileArray, bufferSize);
}

void NotifyTileChanged(Tile* tile, int64_t x, int64_t y, int layer) {
    TILE_BUCKETS[layer].Set(tile, x, y);
}

size_t IW_EXPORT CountChanged(int layer) {
    return TILE_BUCKETS[layer].CountChanges();
}

void SetWorkspace(const char* path) {
    for(auto & bucket : TILE_BUCKETS) {
        bucket.SetWorkspace(path);
    }
}