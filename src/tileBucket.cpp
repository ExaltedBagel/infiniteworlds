//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#pragma once

#include "tileBucket.hpp"
#include "constants.hpp"

#include <array>
#include <algorithm>
#include <iostream>

namespace fs = std::filesystem;

TileBucket::TileBucket(int layer) 
    : _mutex(), _gen(), _workspace(fs::temp_directory_path() / "IW"),
    _baseXCoord(0), _baseYCoord(0), _isInit(false), _layer(layer)
{
    try {
        fs::create_directory(_workspace);
    } 
    catch (fs::filesystem_error e) {

    }
}

void TileBucket::Init() {
    SetScope(0, 0);
}

void TileBucket::Clear() {
    _isInit = false;
    _baseXCoord = 0;
    _baseYCoord = 0;
}

void TileBucket::FetchTiles(Tile* tileArray, size_t bufferSize) {
    if(!_isInit) {
        Init();
    }

    for(const auto& chunk : _loadedChunks) {
        memcpy_s(&tileArray[chunk.bufferOffset], CHUNK_SIZE * sizeof(Tile), 
            &_tilesArray[chunk.bufferOffset], CHUNK_SIZE * sizeof(Tile));
    }
}

void TileBucket::Set(Tile* tile, int64_t x, int64_t y) {
    if(!IsCoordinateLoaded(x, y)) {
        std::cerr << "Tried to modify an unloaded Chunk. Aborting\n";
        return;
    }
    
    int64_t chunkX = GetChunkIndex(x);
    int64_t chunkY = GetChunkIndex(y);

    auto loadedChunkIt = std::find_if(_loadedChunks.begin(), _loadedChunks.end(), 
        [chunkX, chunkY](const LoadedChunk& chunk) {
            return chunk.x == chunkX && chunk.y == chunkY;
        });

    if(loadedChunkIt != _loadedChunks.end()) {
        loadedChunkIt->Set(tile, x, y);
    }
    else {
        std::cerr << "ERROR! Chunk is supposed to be loaded, but was not found!";
    }
}

void TileBucket::SetScope(int64_t chunkX, int64_t chunkY) {
    // Dont rescope if we don't need it.
    if(_isInit && (chunkX == _scopeX) && (chunkY == _scopeY)) {
        return;
    }

    if(!_isInit) {
        for(size_t y = 0; y < CHUNK_Y_COUNT; ++y) {
            size_t loadedChunkOffset = y * CHUNK_X_COUNT;
            size_t yOffset = loadedChunkOffset * CHUNK_SIZE;
            
            for(size_t x = 0; x < CHUNK_X_COUNT; ++x) {
                auto newChunk = LoadedChunk(x + chunkX, y + chunkY, loadedChunkOffset + x, yOffset + x * CHUNK_SIZE);
                _loadedChunks[newChunk.infoOffset] = newChunk;
                _gen.getBaseChunk(newChunk.x, newChunk.y, &_tilesArray[newChunk.bufferOffset]);
                _loadedChunks[newChunk.infoOffset].Load(_workspace, _layer);
            }
        }
    }
    else {
        int64_t dx = chunkX - _scopeX;
        int64_t dy = chunkY - _scopeY;
        // Replace if lower than
        int64_t xBoundLower = chunkX;
        int64_t xBoundUpper = chunkX + CHUNK_X_COUNT;
        int64_t yBoundLower = chunkY;
        int64_t yBoundUpper = chunkY + CHUNK_Y_COUNT;

        auto isDisposable = [xBoundLower, xBoundUpper, yBoundLower, yBoundUpper](const LoadedChunk& chunk) {
            return chunk.x < xBoundLower || chunk.x >= xBoundUpper || chunk.y < yBoundLower || chunk.y >= yBoundUpper;
        };

        std::vector<const LoadedChunk*> disposableChunks;
        for(const auto& loadedChunk : _loadedChunks) {
            if(isDisposable(loadedChunk)) {
                disposableChunks.push_back(&loadedChunk);
            }
        }
        
        std::vector<LoadedChunk> tilesToLoad;
        for(int64_t y = chunkY; y < chunkY + CHUNK_Y_COUNT; ++y) {
            for(int64_t x = chunkX; x < chunkX + CHUNK_X_COUNT; ++x) {
                if(y < _scopeY || y >= _scopeY + CHUNK_Y_COUNT) {
                    tilesToLoad.emplace_back(x, y);
                }
                else if(x < _scopeX || x >= _scopeX + CHUNK_X_COUNT) {
                    tilesToLoad.emplace_back(x, y);
                }
            }
        }

        auto itA = disposableChunks.begin();
        auto itB = tilesToLoad.begin();

        while(itA != disposableChunks.end() || itB != tilesToLoad.end())
        {
            (*itA)->Save(_workspace, _layer);
            itB->infoOffset = (*itA)->infoOffset;
            itB->bufferOffset = (*itA)->bufferOffset;
            _loadedChunks[itB->infoOffset].changes.clear();
            _loadedChunks[itB->infoOffset] = *itB;
            _gen.getBaseChunk(itB->x, itB->y, &_tilesArray[itB->bufferOffset]);
            _loadedChunks[itB->infoOffset].Load(_workspace, _layer);
            for(const auto& change: _loadedChunks[itB->infoOffset].changes) {
                _tilesArray[change.RawOffset()] = change.tile;
            }

            ++itA;
            ++itB;
        }
    }
    
    _scopeX = chunkX;
    _scopeY = chunkY;
    _baseXCoord = chunkX * CHUNK_SIZE;
    _baseYCoord = chunkY * CHUNK_SIZE * CHUNK_X_COUNT;
    _isInit = true;
}

int64_t TileBucket::BaseX() const {
    return _baseXCoord;
}

int64_t TileBucket::BaseY() const {
    return _baseYCoord;
}

size_t TileBucket::CountChanges() const {
    size_t changes = 0;
    for(const auto& loadedChunk : _loadedChunks) {
        changes += loadedChunk.changes.size();
    }
    return changes;
}

void TileBucket::SetWorkspace(const char* path) {
    _workspace = path;
    try {
        fs::create_directory(_workspace);
    } 
    catch (fs::filesystem_error e) {

    }
}

int64_t TileBucket::GetChunkIndex(int64_t x) const {
    return static_cast<int64_t>(
        floor(static_cast<double>(x) / CHUNK_SIZE_1D));
}

bool TileBucket::IsCoordinateLoaded(int64_t x, int64_t y) const {
    return (_baseXCoord <= x && x < (_baseXCoord + CHUNK_SIZE_1D * CHUNK_X_COUNT))
        && (_baseYCoord <= y && y < (_baseYCoord + CHUNK_SIZE_1D * CHUNK_Y_COUNT));
}