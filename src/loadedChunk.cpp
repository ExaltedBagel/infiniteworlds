//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#include "loadedChunk.hpp"

#include <algorithm>
#include <array>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

LoadedChunk::LoadedChunk() 
    : x(0), y(0), infoOffset(0), bufferOffset(0)
{

}

LoadedChunk::LoadedChunk(int64_t x, int64_t y, size_t infoOffset, size_t bufferOffset) 
    : x(x), y(y), infoOffset(infoOffset), bufferOffset(bufferOffset) 
{

}

LoadedChunk::LoadedChunk(const LoadedChunk& other) 
    : x(other.x), y(other.y), infoOffset(other.infoOffset), bufferOffset(other.bufferOffset)
{

}
    
LoadedChunk& LoadedChunk::operator=(const LoadedChunk& other)
{
    x = other.x;
    y = other.y;
    infoOffset = other.infoOffset;
    bufferOffset = other.bufferOffset;
    return *this;
}

void LoadedChunk::Set(Tile* tile, int64_t x, int64_t y) {
    auto isSameTile = [x, y](const TileChange& tileChange) {
        return tileChange.x == x && tileChange.y == y;
    };

    auto it = std::find_if(changes.begin(), changes.end(), isSameTile);

    if(it == changes.end()) {
        changes.emplace_back(*tile, x, y);
    }
    else {
        it->tile = *tile;
    }
}

void LoadedChunk::Save(fs::path rootPath, int layer) const {
    if(changes.size() == 0) {
        return;
    }

    std::stringstream fileName;
    fileName << x << "-" << y << "-layer" << layer << ".dat";
    auto filePath = rootPath / fileName.str();

    std::ofstream wf(filePath.c_str(), std::ios::binary | std::ios::trunc);
    if(!wf) {
        std::cerr << "Could not open file " << filePath;
        return;
    }

    std::array<char, sizeof(TileChange)> buffer;
    for(const auto& change : changes) {
        for(int i = 0; i < buffer.size(); ++i) {
            buffer[i] = '\0';
        }
        change.ToBinary(buffer.data());
        wf.write( buffer.data(), sizeof(change) );
        if(!wf) {
            throw std::runtime_error("Save operation failed.");
        }
    }
}


void LoadedChunk::Load(fs::path rootPath, int layer) {
    std::stringstream fileName;
    fileName << x << "-" << y << "-layer" << layer << ".dat";
    auto filePath = rootPath / fileName.str();
    std::ifstream isf(filePath.c_str(), std::ios::binary);
    if(!isf) {
        return;
    }

    std::array<char, sizeof(TileChange)> rdBuffer;
    for(int i = 0; i < rdBuffer.size(); ++i) {
        rdBuffer[i] = '\0';
    }
    while (isf.read(rdBuffer.data(), sizeof(TileChange))) {
        changes.emplace_back(TileChange(rdBuffer.data()));
    }
}
