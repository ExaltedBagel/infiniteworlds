//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#include "tileChange.hpp"
#include "constants.hpp"

TileChange::TileChange() 
    : tile(), x(0), y(0)
{
    
}

TileChange::TileChange(const TileChange& other)
    : tile(other.tile), x(other.x), y(other.y)
{

}

TileChange::TileChange(const Tile& tile, int64_t x, int64_t y) 
    : tile(tile), x(x), y(y)
{

}

TileChange::TileChange(const char* binInfo) {
    tile = Tile(binInfo[0]);
    memcpy(&x, &binInfo[sizeof(Tile)], sizeof(int64_t));
    memcpy(&y, &binInfo[sizeof(Tile) + sizeof(int64_t)], sizeof(int64_t));
}

void TileChange::ToBinary(char* buffer) const {
    //memcpy(buffer, this, sizeof(TileChange));
    //return;
    tile.ToBinary(buffer);
    memcpy(&buffer[sizeof(Tile)], &x, sizeof(x));
    memcpy(&buffer[sizeof(Tile) + sizeof(y)], &y, sizeof(y));
}

size_t TileChange::RawOffset() const {
    return GetOffsetIndex(x, y);
}

bool TileChange::operator==(const TileChange& other) {
    return x == other.x && y == other.y;
}