//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#pragma once

#include "constants.hpp"
#include "defaultGenerator.h"
#include "tileChange.hpp"
#include "loadedChunk.hpp"

#include <array>
#include <shared_mutex>
#include <string>
#include <filesystem>
#include <vector>

class TileBucket {
public:
    TileBucket(int layer);

    // Base stuff
    void Init();
    void Clear();
    void FetchTiles(Tile* tileArray, size_t bufferSize);
    void SetScope(int64_t chunkX, int64_t chunkY);
    int64_t BaseX() const;
    int64_t BaseY() const;
    size_t CountChanges() const;

    // Tile operations
    void Set(Tile* tile, int64_t x, int64_t y);

    // File handling
    void SetWorkspace(const char* path);

private:
    int64_t GetChunkIndex(int64_t x) const;
    bool IsCoordinateLoaded(int64_t x, int64_t y) const;

    mutable std::shared_mutex _mutex;
    DefaultGenerator _gen;

    bool _isInit = false;
    std::array<Tile, TILE_ARRAY_SIZE> _tilesArray;
    std::array<LoadedChunk, CHUNK_X_COUNT * CHUNK_Y_COUNT> _loadedChunks;

    std::filesystem::path _workspace;

    int64_t _scopeX;
    int64_t _scopeY;
    int64_t _baseXCoord;
    int64_t _baseYCoord;
    int _layer;
};