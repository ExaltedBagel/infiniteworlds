//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#include "defaultGenerator.h"
#include "constants.hpp"
#include <iostream>

DefaultGenerator::DefaultGenerator() 
    : _seed(123456u), _perlin(_seed) 
{

}

void DefaultGenerator::getBaseChunk(int64_t tX, int64_t tY, Tile* outBuffer) {
    constexpr double xStride = 0.1234;
    constexpr double yStride = 0.1982;
    constexpr double cutoff = 0.5;

    for(size_t y = 0; y < CHUNK_SIZE_1D; ++y) {
        size_t baseYIndex = y * CHUNK_SIZE_1D;
        int64_t baseYNoiseIndex = (tY + y) * CHUNK_SIZE_1D;
        auto yMult = baseYNoiseIndex * yStride;

        for(size_t x = 0; x < CHUNK_SIZE_1D; ++x) {
            auto value = _perlin.octave2D_01((x + tX) * xStride, yMult, 4);
            outBuffer[baseYIndex + x].content = (value > cutoff);
        }
    }
}