# Infinite Worlds
Infinite worlds is a framework to help jumpstart projects relying on infite procedural generations.
It uses a deterministic generation algorithm to create a base world, and keeps track of 
changes which were committed to the world.  


# Project Status
**MVP** (A lot of features are missing at the moment. See upcoming features section.)  
Also, the code is disgusting in some places. This was thrown together in 2-3 evenings.


# Requirements
- C++ 20 compiler
- CMake 3.21+

# Tested On
- MSVC 19.29.30139.0


# Features
- Basic tile and chunk system
- Chunk change serialization
- Handling of area load and unload
- Low memory footprint


## Current Limitations
- Serialization is a too coupled to make it trivial to define own Tile class


# License
InfiniteWorlds is distributed under the **MIT License**


# Usage
```
#include "infiniteWorlds.h"

int main() {
    // Create your buffer
    Tile* tiles = new Tile[TILE_ARRAY_SIZE];

    // Set the engine workspace (where chunks get saved and loaded. Defaults to temp directory)
    SetWorkspace("C:\Temp\IW");

    // Set the scope the chunk square with bottom-left coord {0,0}
    SetScope(0, 0);

    // Fetch the tiles
    FetchTiles(tiles, sizeof(tiles) /*Buffer size currently unused*/);

    // Modify a tile
    tileArray[1].content = false;
    NotifyTileChanged(&tileArray[1], 1, 0);

    return 0;
}

```

# Upcoming Features
- Disk writes on a seperate thread
- Better CMakeLists usage (compile options)
- Serilization interface to make use defined Tiles easier to use
- Static asserts and security review
- Interface to register generation method