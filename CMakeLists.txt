cmake_minimum_required(VERSION 3.21)

# set the project name
project(InfiniteWorlds)

set(CMAKE_CXX_STANDARD 20)
set(WINDOWS_EXPORT_ALL_SYMBOLS On)

message(${CMAKE_CXX_COMPILER_ID})
message(${CMAKE_CXX_COMPILER_VERSION})

# Define this if you want bigger chunks
add_compile_definitions(DEF_CHUNK_POWER=8)

add_subdirectory(src)

target_include_directories(IW PUBLIC "include" ${CMAKE_BINARY_DIR})

include(GenerateExportHeader)
GENERATE_EXPORT_HEADER(IW)

add_subdirectory(tests)

