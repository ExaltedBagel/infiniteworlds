//----------------------------------------------------------------------------------------
//
//	InfiniteWorlds
//	Infinite procedural world generation framework
//
//	Copyright (C) 2022 Maxime Jacob <xltdcode@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//	
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//	
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

#pragma once

#include "tile.h"
#include <type_traits>

#ifndef DEF_CHUNK_POWER
    #define DEF_CHUNK_POWER 8
#endif

#ifndef DEF_MAP_LAYERS
    #define DEF_MAP_LAYERS 4
#endif

template<typename T, typename U>
size_t constexpr const_pow(T base, U exponent) {
    static_assert(std::is_integral<U>(), "exponent must be integral");
    return exponent == 0 ? 1 : base * const_pow(base, exponent - 1);
}

constexpr int64_t CHUNK_X_COUNT = 8;
constexpr int64_t CHUNK_Y_COUNT = 8;

constexpr int64_t CHUNK_SIZE_1D = const_pow<int, int>(2, DEF_CHUNK_POWER / 2);
constexpr int64_t CHUNK_SIZE = const_pow<int, int>(2, DEF_CHUNK_POWER);
constexpr int64_t TILE_ARRAY_SIZE = CHUNK_SIZE * CHUNK_X_COUNT * CHUNK_Y_COUNT;
constexpr size_t DATA_BUFFER_SIZE = CHUNK_SIZE * CHUNK_X_COUNT * CHUNK_Y_COUNT * sizeof(Tile);

constexpr size_t GetOffsetIndex(size_t x, size_t y) {
    return y * CHUNK_SIZE_1D + x;
}